angular.module('starter.controllers', [])

  .controller('Page1Ctrl', function($scope,$http) {
    $http.get('http://52.76.85.10/test/location.json').success(function(res){
      console.log(res);
      $scope.listData = res;
    }).error(function(error){
      console.log(error);
    });
  })
  .controller('Page2Ctrl', function($scope,$stateParams) {
    $scope.parampage2 = $stateParams.param;
  })

;
